<?php

namespace Drupal\entity_preprocess_services\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

/**
 * This class registers entity preprocess services.
 *
 * @package Drupal\entity_preprocess_services\Compiler
 */
class EntityPreprocessPass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container) {
    if (!$container->hasDefinition('entity_preprocess_services.manager')) {
      return;
    }
    // Add services tagged 'access_check' to the access_manager service.
    $entityPreprocessManager = $container->getDefinition(
      'entity_preprocess_services.manager'
    );

    $taggedServices = $container->findTaggedServiceIds(
      'entity_preprocess_service'
    );

    if (empty($taggedServices)) {
      return;
    }

    foreach ($taggedServices as $id => $attributes) {
      $taggedService = $container->getDefinition($id);

      if (!$taggedService instanceof Definition) {
        continue;
      }

      $properties = $taggedService->getProperties();

      if (empty($properties['applies_to'])) {
        continue;
      }

      $excludes = $properties['excludes'] ?? [];

      foreach ($properties['applies_to'] as $property) {
        $entityType = $property['entity_type'] ?? NULL;

        if (empty($entityType)) {
          continue;
        }

        $tag = $taggedService->getTag('entity_preprocess_service');
        $priority = $tag[0]['priority'] ?? 0;

        $serviceDefinition = [
          'service' => $id,
          'entity_type' => $entityType,
          'bundle' => $property['bundle'] ?? NULL,
          'view_mode' => $property['view_mode'] ?? NULL,
          'excludes' => $excludes,
          'priority' => (int) $priority,
        ];

        $serviceDefinitions[] = $serviceDefinition;
      }
    }

    if (isset($serviceDefinitions)) {
      uasort($serviceDefinitions, function ($a, $b) {
        return $a['priority'] <=> $b['priority'];
      });

      $entityPreprocessManager->addMethodCall(
        'addEntityPreprocessServices',
        [$serviceDefinitions]
      );
    }
  }

}
