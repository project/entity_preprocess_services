<?php

namespace Drupal\entity_preprocess_services;

use Drupal\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_preprocess_services\PreprocessService\PreprocessServiceInterface;

/**
 * Class that manages the entity preprocess services.
 *
 * @package Drupal\entity_preprocess_services
 */
class EntityPreprocessServicesManager {

  /**
   * Builds an EntityPreprocessServicesManager object.
   *
   * @param \Drupal\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   */
  public function __construct(protected ContainerInterface $container) {
  }

  /**
   * All entity preprocess services.
   *
   * @var array
   *   An array containing the preprocess services.
   */
  protected $entityPreprocessServices = [];

  /**
   * Adds a entity preprocess service definition to our array of services.
   *
   * @param array $serviceDefinitions
   *   The service definitions.
   */
  public function addEntityPreprocessServices(array $serviceDefinitions) {
    $this->entityPreprocessServices = $serviceDefinitions;
  }

  /**
   * Loads the entity preprocess services.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $viewMode
   *   The view mode to preprocess.
   *
   * @return array
   *   The services found for this entity.
   *
   * @throws \Exception
   *   When a service does not implement the correct interface.
   */
  public function getEntityPreprocessServices(EntityInterface $entity, string $viewMode) : array {
    $matchingServices = [];

    $entityTypeId = $entity->getEntityTypeId();
    $entityBundle = $entity->bundle();

    foreach ($this->entityPreprocessServices as $serviceDefinition) {
      // Lets see if there are excludes in this service definition.
      if (!empty($serviceDefinition['excludes'])) {
        foreach ($serviceDefinition['excludes'] as $excludeDefinition) {
          if ($this->serviceDefinitionMatches($excludeDefinition, $entity, $viewMode)) {
            continue 2;
          }
        }
      }

      // This service does not match the entity type.
      if ($serviceDefinition['entity_type'] !== $entityTypeId) {
        continue;
      }

      // This service has a bundle defined and the bundle does not match.
      // Meaning we can skip this service.
      if ($serviceDefinition['bundle'] && $serviceDefinition['bundle'] !== $entityBundle) {
        continue;
      }

      // This service has a view mode defined and the view mode does not match.
      // Meaning we can skip this service.
      if ($serviceDefinition['view_mode'] && $serviceDefinition['view_mode'] !== $viewMode) {
        continue;
      }

      $preprocessService = $this->loadPreprocessService($serviceDefinition['service']);

      $matchingServices[] = $preprocessService
        ->setEntity($entity)
        ->setViewMode($viewMode);
    }

    return $matchingServices;
  }

  /**
   * Does our service definition match our entity?
   *
   * @param array $serviceDefinition
   *   The service definition.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $viewMode
   *   The view mode.
   *
   * @return bool
   *   Do we have a match?
   */
  protected function serviceDefinitionMatches(array $serviceDefinition, EntityInterface $entity, string $viewMode): bool {
    $entityTypeId = $entity->getEntityTypeId();
    $entityBundle = $entity->bundle();

    if ($serviceDefinition['entity_type'] !== $entityTypeId) {
      return FALSE;
    }

    // This service has a bundle defined and the bundle does not match.
    // Meaning we can skip this service.
    if ($serviceDefinition['bundle'] && $serviceDefinition['bundle'] !== $entityBundle) {
      return FALSE;
    }

    // This service has a view mode defined and the view mode does not match.
    // Meaning we can skip this service.
    if ($serviceDefinition['view_mode'] && $serviceDefinition['view_mode'] !== $viewMode) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Loads a preprocess service by id.
   *
   * @param string $id
   *   The id of the service.
   *
   * @return \Drupal\entity_preprocess_services\PreprocessService\PreprocessServiceInterface
   *   The preprocess service interface.
   *
   * @throws \Exception
   *   When service does not implement PreprocessServiceInterface.
   */
  protected function loadPreprocessService(string $id) : PreprocessServiceInterface {
    $preprocessService = $this->container->get($id);

    if (!$preprocessService instanceof PreprocessServiceInterface) {
      throw new \Exception(
        sprintf('The preprocess service must implement %s', PreprocessServiceInterface::class)
      );
    }

    return $preprocessService;
  }

}
