<?php

namespace Drupal\entity_preprocess_services;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\entity_preprocess_services\DependencyInjection\Compiler\EntityPreprocessPass;

/**
 * Class that adds the EntityPreprocessPass compiler pass.
 */
class EntityPreprocessServicesServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    // Add a compiler pass to discover all data collector services.
    $container->addCompilerPass(new EntityPreprocessPass());

  }

}
