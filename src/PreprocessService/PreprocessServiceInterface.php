<?php

namespace Drupal\entity_preprocess_services\PreprocessService;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityInterface;

/**
 * Interface that each preprocess service should implement.
 *
 * @package Drupal\entity_preprocess_services\PreprocessService
 */
interface PreprocessServiceInterface extends CacheableDependencyInterface {

  /**
   * Preprocesses the variables.
   *
   * @return array
   *   The variables.
   */
  public function preprocess() : array;

  /**
   * Sets the entity property.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\entity_preprocess_services\PreprocessService\PreprocessServiceInterface
   *   Current preprocess service.
   */
  public function setEntity(EntityInterface $entity) : PreprocessServiceInterface;

  /**
   * Sets the view mode.
   *
   * @param string $viewMode
   *   The view mode.
   *
   * @return \Drupal\entity_preprocess_services\PreprocessService\PreprocessServiceInterface
   *   Current preprocess service.
   */
  public function setViewMode(string $viewMode) : PreprocessServiceInterface;

  /**
   * Sets the preprocess variables.
   *
   * @param array $variables
   *   The variables.
   *
   * @return \Drupal\entity_preprocess_services\PreprocessService\PreprocessServiceInterface
   *   Current preprocess service.
   */
  public function setVariables(array &$variables) : PreprocessServiceInterface;

  /**
   * Sets the cacheable metadata.
   *
   * @param \Drupal\Core\Cache\CacheableMetadata $cacheableMetadata
   *   The cacheable metadata.
   *
   * @return \Drupal\entity_preprocess_services\PreprocessService
   *   Current preprocess service.
   */
  public function setCacheableMetadata(CacheableMetadata $cacheableMetadata): PreprocessServiceInterface;

}
