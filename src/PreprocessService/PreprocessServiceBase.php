<?php

namespace Drupal\entity_preprocess_services\PreprocessService;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityInterface;

/**
 * Base class for preprocess services.
 *
 * @package Drupal\entity_preprocess_services\PreprocessService
 */
abstract class PreprocessServiceBase implements PreprocessServiceInterface {

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The view mode.
   *
   * @var string
   */
  protected $viewMode;

  /**
   * The variables.
   *
   * @var array
   */
  protected $variables;

  /**
   * The cacheable metadata.
   *
   * @var \Drupal\Core\Cache\CacheableMetadata
   */
  protected $cacheableMetadata;

  /**
   * The list of entities the service is called for.
   *
   * @var array
   */
  // phpcs:ignore
  public $applies_to;

  /**
   * {@inheritdoc}
   */
  public function preprocess(): array {
    return $this->variables;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity(EntityInterface $entity): PreprocessServiceInterface {
    $this->entity = $entity;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setViewMode(string $viewMode): PreprocessServiceInterface {
    $this->viewMode = $viewMode;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setVariables(array &$variables): PreprocessServiceInterface {
    $this->variables = $variables;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCacheableMetadata(CacheableMetadata $cacheableMetadata): PreprocessServiceInterface {
    $this->cacheableMetadata = $cacheableMetadata;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return CacheBackendInterface::CACHE_PERMANENT;
  }

}
