<?php

/**
 * @file
 * The main module file for the entity_preprocess_services.
 */

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function entity_preprocess_services_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.entity_preprocess_services':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module provides a way for developers to use services instead of hooks for preprocess services.') . '</p>';
      $output .= '<p>' . t('See the project page for more information.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_preprocess_node().
 */
function entity_preprocess_services_preprocess_node(array &$variables) {
  /** @var \Drupal\node\NodeInterface $node */
  $node = $variables['node'];

  _entity_preprocess_services_preprocess_entity(
    $variables,
    $node,
    $variables['elements']['#view_mode']
  );
}

/**
 * Implements hook_preprocess_paragraph().
 */
function entity_preprocess_services_preprocess_paragraph(array &$variables) {
  $paragraph = $variables['paragraph'];

  _entity_preprocess_services_preprocess_entity(
    $variables,
    $paragraph,
    $variables['elements']['#view_mode']
  );
}

/**
 * Preprocesses our entity.
 *
 * @param array $variables
 *   Current preprocess variables by reference.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   Our entity to preprocess.
 * @param string $viewMode
 *   The view mode to preprocess in.
 *
 * @throws \Exception
 *   When our service does not implement the correct interface.
 */
function _entity_preprocess_services_preprocess_entity(
  array &$variables,
  EntityInterface $entity,
  string $viewMode,
) {
  /** @var \Drupal\entity_preprocess_services\EntityPreprocessServicesManager $manager */
  $manager = \Drupal::service('entity_preprocess_services.manager');
  $services = $manager->getEntityPreprocessServices($entity, $viewMode);

  if (empty($services)) {
    return;
  }

  foreach ($services as $service) {
    $cacheableMetadata = new CacheableMetadata();

    /** @var \Drupal\entity_preprocess_services\PreprocessService\PreprocessServiceInterface $service */
    $service->setVariables($variables);
    $service->setCacheableMetadata($cacheableMetadata);
    $variables = $service->preprocess();
    $cacheableMetadata->addCacheableDependency($service);

    $variablesMetadata = CacheableMetadata::createFromRenderArray($variables);
    $cacheableMetadata->merge($variablesMetadata)->applyTo($variables);
  }
}
