<?php

namespace Drupal\entity_preprocess_services_example\PreprocessService;

use Drupal\entity_preprocess_services\PreprocessService\PreprocessServiceBase;

/**
 * Class ExampleNodePreprocessService.
 *
 * A basic example of a node preprocess service.
 *
 * @package Drupal\entity_preprocess_services_example\PreprocessService
 */
class ExampleNodePreprocessService extends PreprocessServiceBase {

  /**
   * {@inheritdoc}
   */
  public function preprocess(): array {
    $this->preprocessWelcomeMessage();
    return parent::preprocess();
  }

  /**
   * Sets an example variable on a node.
   */
  protected function preprocessWelcomeMessage() {
    $this->variables['welcome'] = 'A basic example of a node preprocess service.';
  }

}
