<?php

namespace Drupal\entity_preprocess_services_example\PreprocessService;

use Drupal\entity_preprocess_services\PreprocessService\PreprocessServiceBase;

/**
 * Class ExampleNodePreprocessService.
 *
 * A basic example of a node preprocess service for nodes of type page.
 *
 * @package Drupal\entity_preprocess_services_example\PreprocessService
 */
class ExampleNodePagePreprocessService extends PreprocessServiceBase {

  /**
   * {@inheritdoc}
   */
  public function preprocess(): array {
    $this->preprocessInfoMessage();
    return parent::preprocess();
  }

  /**
   * Sets an info variable on a node.
   */
  protected function preprocessInfoMessage() {
    $this->variables['info_text'] = 'A basic example of a node preprocess service for nodes of type page.';
  }

}
