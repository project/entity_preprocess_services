CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Developers
 * Maintainers

INTRODUCTION
------------
This module provides a way to use services when preprocess entities.
It does this by adding a service pass and provider.

It is built for developers and doesn't add any special functionality
out of the box.

The module works out of the box for nodes and paragraphs.
But can easily be used for all entities.

You just have to call the following function:
_entity_preprocess_services_preprocess_entity

See configuration on how to use this.

REQUIREMENTS
------------
- php >= 7.1
- Drupal >= 8.5

INSTALLATION
------------
Install the "Entity preprocess services" module as
you would normally install a contributed Drupal module.

Visit https://www.drupal.org/node/1897420 for further
information.

CONFIGURATION
-------------
You can add preprocess services for nodes and paragraphs out of the box.
A preprocess service will be picked up by "tagging" it.

See example module file on how this works:
entity_preprocess_services_example.services.yml

You tag a service as a "preprocess service" by adding this to your service:
```
tags:
  - { name: entity_preprocess_service, priority: 100 }
```
Priority can be used to establish the order of preprocess services.

You can then specify for which entities this service must be called.
You do this by adding following to your service:
```
properties:
      applies_to:
        - { entity_type: 'node', view_mode: 'full'}
```

There are 3 properties that can be added:
- entity_type: type of entity (getEntityTypeId())
- bundle: for what bundle? (bundle())
- view_mode: for what view mode?

A full service would look like this:
```
entity_preprocess_services_example.preprocess_service.node.full:
class: Drupal\entity_preprocess_services_example\
PreprocessService\ExampleNodeFullPreprocessService
tags:
  - { name: entity_preprocess_service, priority: 100 }
properties:
  applies_to:
    - { entity_type: 'node', view_mode: 'full', bundle: 'page'}
```
This service will call the preprocess function for nodes of type page in
the full view mode.

Out of the box, it only works for nodes and paragraphs.
However, you can quickly use this for other / entities or custom entities.

Simply call the function: _entity_preprocess_services_preprocess_entity

Pass 3 arguments:
- the variables array
- the entity
- the view mode

The function will do the rest.

```
/**
 * Implements hook_preprocess_paragraph().
 */
function entity_preprocess_services_preprocess_paragraph(array &$variables) {
  /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
  $paragraph = $variables['paragraph'];

  _entity_preprocess_services_preprocess_entity(
    $variables,
    $paragraph,
    $variables['elements']['#view_mode']
  );
}
```

EXCLUDING ENTITIES
----------
It's possible to exclude certain entity types from a service.
You can do this with the "excludes" key under properties.

For example, you want to preprocess all nodes except "news".
Your service definition can look as follows:
```
entity_preprocess_services_example.preprocess_service.node.full:
class: Drupal\entity_preprocess_services_example\
PreprocessService\ExampleNodeFullPreprocessService
tags:
  - { name: entity_preprocess_service, priority: 100 }
properties:
  applies_to:
    - { entity_type: 'node', view_mode: 'full', bundle: 'page'}
  excludes:
   -  { entity_type: 'node',  bundle: 'news'}
```
View modes can also be ignored.

DEVELOPERS
----------
* Maarten Heip - https://www.drupal.org/u/mheip

But a special thanks to Intracto, for allowing me to do this on company time.
https://www.intracto.com

MAINTAINERS
-----------
Current maintainers:
  * Maarten Heip - https://www.drupal.org/u/mheip
